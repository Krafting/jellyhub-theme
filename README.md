# Jellyhub Theme - A Jellyfin custom CSS theme

![jellyhub-theme/images/logo-jh.png](jellyhub-theme/images/logo-jh.png)

## Sections

[Informations](#informations)

[Screenshots](#screenshots)

[Installation](#installation)

[Custom JellyHub Logo](#custom-jellyhub-logo)

[Custom Accent Color](#custom-accent-color)

[Custom Pre-Roll](#custom-pre-roll)

[To-Do](#to-do)

## Informations

This theme has been customized by me using the [UltraChromic Jellyfin](https://github.com/CTalvio/Ultrachromic) theme.

Why make this exists ? Well, it just started as a private joke and now I'm sharing it for everyone to use as it is in a usable state! 

## Screenshots

![jellyhub-theme/images/0.png](jellyhub-theme/images/0.png)

![jellyhub-theme/images/1.png](jellyhub-theme/images/1.png)

![jellyhub-theme/images/2.png](jellyhub-theme/images/2.png)

## Installation

To install this theme, navigate to the `General` section in the settings.

You can then copy and paste the content of the [`jellyhub.css`](https://gitlab.com/Krafting/jellyhub-theme/-/raw/main/jellyhub-theme/jellyhub.css) inside the `Custom CSS code` box situated in Administration Dashboard > General

### Custom JellyHub Logo

To put the theme logo instead of the Jellyfin default logo, you'll need to put the [`images/logo-jh.png`](jellyhub-theme/images/logo-jh.png) file inside the `/usr/share/jellyfin/web/` folder of your jellyfin installation

You can do that on linux using this command (Note that you'll need root privileges):

`curl -o /usr/share/jellyfin/web/logo-jh.png https://gitlab.com/Krafting/jellyhub-theme/-/raw/main/jellyhub-theme/images/logo-jh.png`

And then add this custom CSS at the end of the custom CSS:

```CSS
.pageTitleWithDefaultLogo {
    background-image: url(logo-jh.png);
}
```

### Custom Accent Color

If you want to replace the orange with a custom color, you can replace line 2 of the custom CSS as follow, using the RR, GG, BB format:
```CSS
    --accent: 24, 25, 26;
```

### Custom Pre-Roll

I've made a custom pre-roll that can be used with the [Intros plugin](https://github.com/BrianCArnold/jellyfin-plugin-intros)

You can find the video in this repository in [jellyhub-theme/prerolls/JellyHub.webm](jellyhub-theme/prerolls/JellyHub.webm)

## To-Do

This to do if I find time

* Custom Favicon
* Custom Login Page Background (Image)
